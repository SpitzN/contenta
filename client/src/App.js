import React, { Component } from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { BrowserRouter as Router, Route } from "react-router-dom";
import NavBar from "./Components/NavBar";
import User from "./Components/User";
import Data from "./Components/Data";
import "typeface-roboto";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#1976D2"
    },
    secondary: {
      main: "#E23237"
    }
  },
  typography: {
    useNextVariants: true
  }
});

class App extends Component {
  render() {
    return (
      <Router>
        <MuiThemeProvider theme={theme}>
          <NavBar />
          <Route exact path="/" component={User} />
          <Route exact path="/dash" component={Data} />
        </MuiThemeProvider>
      </Router>
    );
  }
}

export default App;
