import { observable, action, computed } from "mobx";
import axios from "axios";

export class GeneralStore {
  @observable
  userName = "";

  @observable
  message = "";

  @observable
  modalOpen = false;

  @observable
  loading = false;

  @observable
  success = false;

  @observable
  userMessages = [];

  @observable
  data = [];

  @computed
  get countOfusers() {
    return this.data.length;
  }

  @action handleInput = (name, value) => {
    this[name] = value;
  };

  @action handleOpen = userId => {
    let user = this.data.find(u => userId === u._id);
    this.userName = user.userName;
    this.userMessages = user.messages;
    this.modalOpen = true;
  };

  @action handleClose = () => {
    this.userName = "";
    this.userMessages = [];
    this.modalOpen = false;
  };

  @action syncData = async () => {
    if (!this.loading) {
      this.success = false;
      this.loading = true;
      let response = await axios.get(`/data`);
      this.data = response.data;
      if (this.data.length > 0) {
        this.success = true;
        this.loading = false;
      }
    }

    return;
  };

  updateUser = async (userName, message) => {
    try {
      await axios.put(`/user/${userName}`, { message });
    } catch (err) {
      return;
    }
  };

  createUser = async newUser => {
    try {
      await axios.post(`/user`, newUser);
      return;
    } catch (err) {
      console.log(err);
    }
  };

  checkDBForUser = async userName => {
    let response = await axios.get(`/login/${userName}`);
    return !!response.data.userName;
  };

  @action submitMessage = async (userName, message) => {
    if (userName === "") {
      alert("Please Insert Your User Name");
      return;
    }
    if (message === "") {
      alert("Please Insert Your Message");
      return;
    }
    let checkDB = await this.checkDBForUser(userName);
    if (checkDB) {
      this.updateUser(userName, message);
    } else {
      let newUser = {
        userName: userName,
        messages: [message]
      };
      this.createUser(newUser);
    }
    this.userName = "";
    this.message = "";
  };
}
