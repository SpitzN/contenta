import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: "absolute",
    width: 250,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: "none"
  },
  label: {
    textTransform: "capitalize"
  }
}));

export default function SimpleModal(props) {
  const [modalStyle] = React.useState(getModalStyle);

  const handleClose = () => {
    props.close();
  };

  const classes = useStyles();
  let { name, messages } = props;

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={props.open}
      onClose={handleClose}
    >
      <div style={modalStyle} className={classes.paper}>
        <Typography
          variant="h6"
          align="center"
          id="modal-title"
          className={classes.label}
        >
          {name}
        </Typography>
        {messages.map((m, i) => (
          <Typography
            key={i}
            variant="subtitle1"
            align="center"
            className={classes.label}
            id="simple-modal-description"
          >
            {m}
          </Typography>
        ))}
      </div>
    </Modal>
  );
}
