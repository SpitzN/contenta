import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { observer, inject } from "mobx-react";

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(2)
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 500,
    textAlign: "center"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300
  },
  submit: {
    margin: theme.spacing(3, 3),
    width: 150
  }
});

@inject("GeneralStore")
@observer
class User extends Component {
  handleChange = e => {
    let { GeneralStore } = this.props;
    GeneralStore.handleInput(e.target.name, e.target.value);
  };

  submitMessage = () => {
    let { GeneralStore } = this.props;
    GeneralStore.submitMessage(GeneralStore.userName, GeneralStore.message);
  };

  render() {
    const { classes } = this.props;
    let { GeneralStore } = this.props;
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <TextField
                  className={classes.textField}
                  placeholder="User Name"
                  onChange={this.handleChange}
                  type="text"
                  name="userName"
                  value={GeneralStore.userName}
                />
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <TextField
                  className={classes.textField}
                  placeholder="Message"
                  onChange={this.handleChange}
                  type="text"
                  name="message"
                  value={GeneralStore.message}
                />
              </Grid>
            </Grid>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.submitMessage}
                  className={classes.submit}
                >
                  Submit
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(User);
