import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Fab from "@material-ui/core/Fab";
import SyncIcon from "@material-ui/icons/Sync";
import FaceIcon from "@material-ui/icons/Face";
import CheckIcon from "@material-ui/icons/Check";
import CircularProgress from "@material-ui/core/CircularProgress";
import clsx from "clsx";
import { green } from "@material-ui/core/colors";
import { observer, inject } from "mobx-react";
import SimpleModal from "./Modal";
import "typeface-roboto";
import moment from "moment";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 300
  },
  tableRow: {
    curser: "pointer"
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700]
    }
  },
  fabProgress: {
    color: green[500],
    position: "absolute",
    top: -6,
    left: -6,
    zIndex: 1
  },
  label: {
    textTransform: "capitalize",
    position: "relative"
  }
});

@inject("GeneralStore")
@observer
class Data extends Component {
  sync = () => {
    let { GeneralStore } = this.props;
    GeneralStore.syncData();
  };

  handleOpen = event => {
    let userId = event.target.parentNode.id;
    this.props.GeneralStore.handleOpen(userId);
  };

  render() {
    let { GeneralStore } = this.props;
    let countOfUsers = GeneralStore.data.length;
    let success = GeneralStore.success;
    let loading = GeneralStore.loading;
    const { classes } = this.props;
    const buttonClassname = clsx({
      [classes.buttonSuccess]: GeneralStore.success
    });

    return (
      <Paper className={classes.root}>
        <div className={classes.wrapper}>
          <Fab
            color="primary"
            aria-label="Sync"
            size="large"
            className={buttonClassname}
            onClick={this.sync}
          >
            {success ? <CheckIcon /> : <SyncIcon />}
          </Fab>
          {loading && (
            <CircularProgress size={68} className={classes.fabProgress} />
          )}
        </div>
        {countOfUsers > 0 ? (
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell variant="head" align="center">
                  User Name
                </TableCell>
                <TableCell variant="head" align="center">
                  Last Message
                </TableCell>
                <TableCell variant="head" align="center">
                  Date updated
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <SimpleModal
                name={GeneralStore.userName}
                messages={GeneralStore.userMessages}
                open={this.props.GeneralStore.modalOpen}
                close={this.props.GeneralStore.handleClose}
              />
              {GeneralStore.data.map(row => (
                <TableRow
                  key={row.userName}
                  hover
                  id={row._id}
                  className={classes.tableRow}
                  onClick={this.handleOpen}
                >
                  <TableCell
                    variant="body"
                    align="center"
                    className={classes.label}
                  >
                    <FaceIcon /> {row.userName}
                  </TableCell>
                  <TableCell
                    align="center"
                    className={classes.label}
                    variant="body"
                  >
                    {row.messages.slice(-1)[0]}
                  </TableCell>
                  <TableCell align="center">
                    {moment(row.last_update).format("MMMM Do YYYY, h:mm a")}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        ) : (
          <Typography
            variant="h6"
            align="center"
            id="sync-message"
            className={classes.label}
          >
            {`press the sync button to load Data`}
            <SyncIcon />
          </Typography>
        )}
      </Paper>
    );
  }
}

export default withStyles(styles)(Data);
