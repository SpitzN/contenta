const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  userName: String,
  messages: [],
  date_created: { type: Date, default: Date.now },
  last_update: { type: Date, default: Date.now }
});

module.exports = User = mongoose.model("User", UserSchema);
