const express = require("express");
const router = express.Router();
const User = require("../model/User");

router.get(`/sanity`, function(req, res) {
  res.send(`hello from the other side`);
});

router.get(`/data`, (req, res) => {
  User.find()
    .then(users => res.send(users))
    .catch(err => {
      console.log(err);
      throw new Error(err);
    });
});

router.get(`/user/:id`, async (req, res) => {
  let userId = req.params.id;
  let user = await User.findById(userId);
  if (!user) throw new Error(`No user found`);
  res.send(user);
});

router.get(`/login/:userName`, async (req, res) => {
  let name = req.params.userName;
  let user = await User.findOne({ userName: name });
  res.send(user);
});

router.post(`/user`, async (req, res) => {
  let user = req.body;
  let newUser = await new User({
    userName: user.userName,
    messages: user.messages
  });
  newUser
    .save()
    .then(() => res.send({ success: true }))
    .catch(err => res.status(404).json({ success: false }));
});

router.put(`/user/:userName`, async (req, res) => {
  let name = req.params.userName;
  let message = req.body.message;
  let last_update = Date.now();
  let user = await User.findOne({ userName: name });
  if (!user) throw new Error(`No user found`);
  user.messages.push(message);
  user.last_update = last_update;
  user.save();
  res.send(user);
});

module.exports = router;
